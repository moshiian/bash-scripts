#!/bin/bash
PROJECT_DIR="$1"
MVN="/usr/local/mvn/apache-maven-3.3.1/bin/mvn"
MAVEN_VERSIONS_PLUGIN="org.codehaus.mojo:versions-maven-plugin:1.3.1"
MAVEN_VERSIONS_PLUGIN_SET_GOAL="${MAVEN_VERSIONS_PLUGIN}:set -DgenerateBackupPoms=false"
MAVEN_VERSIONS_PLUGIN_UPDATE_PARENT_GOAL="${MAVEN_VERSIONS_PLUGIN}:update-parent -DgenerateBackupPoms=false -DallowSnapshots=true"
MAVEN_VERSIONS_PLUGIN_UPDATE_DEPENDENCIES_GOAL="${MAVEN_VERSIONS_PLUGIN}:use-latest-versions -DgenerateBackupPoms=false -DallowSnapshots=false"
MAVEN_HELP_PLUGIN="org.apache.maven.plugins:maven-help-plugin:2.1.1"
MAVEN_HELP_PLUGIN_EVALUATE_VERSION_GOAL="${MAVEN_HELP_PLUGIN}:evaluate -Dexpression=project.version"
GRADLE=/usr/local/gradle/gradle-4.4/bin/gradle
DRY_RUN=false
ALLOW_OUTSIDE_JENKINS=false
SKIP_BRANCH_SWITCH=false
SVN=/usr/bin/svn
GIT=/usr/bin/git
SVN_LOGIN="builder"
SVN_PASS="sedatora"
FIND=/usr/bin/find
#####################################################################################################################
function defineBuilder()
{
	cd $PROJECT_DIR
	local builder='none'

	if [ -f pom.xml ] ; then
		builder='mvn'
	elif [ -f build.gradle ]; then
		builder='gradle'
	fi

    echo "$builder"
}
#####################################################################################################################
function validatePomExists() {
  cd $PROJECT_DIR
  if [ -f pom.xml ] ; then
    echo "Found pom.xml file: [${PROJECT_DIR}/pom.xml]"
  else
    echo "ERROR: No pom.xml file detected in current directory [${PROJECT_DIR}]. Exiting script with error status."
    exit 50
  fi
}
#####################################################################################################################
function validatePom() {
  $MVN validate
  STATUS=`echo $?`
  if [ ${STATUS} -ne 0 ] ; then
    echo "ERROR: Maven POM did not validate successfully. Exiting script with error status."
    exit 40
  fi
}
#####################################################################################################################
function initMvnCurrentProjectVersion() {
  cd $PROJECT_DIR
  CURRENT_PROJECT_VERSION=`${MVN} org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.version | egrep '^[0-9\.]*(-SNAPSHOT)?$'`

  if [ -z ${CURRENT_PROJECT_VERSION} ] ; then
    echo "  ERROR: Couldn't detect current version. Validating pom in case there was a validation issue."
    validatePom
    echo "  ERROR: Couldn't detect current version. Exiting with error status."
    exit 20
  else
    echo "  Version found: [${CURRENT_PROJECT_VERSION}]"
  fi
}
#####################################################################################################################
function initMvnNextProjectVersion() {
  local CLEANED=`echo ${CURRENT_PROJECT_VERSION} | sed -e 's/[^0-9][^0-9]*$//'`
  local CURRENT_BUILD_NUMBER=`echo ${CLEANED} | sed -e 's/[0-9]*\.//g'`
  local NEXT_BUILD_NUMBER=`expr ${CURRENT_BUILD_NUMBER} + 1`

  echo "Sanitized current project version: [${CLEANED}]"
  echo "Current build number in project version: [${CURRENT_BUILD_NUMBER}]"
  echo "Calculated next build number: [${NEXT_BUILD_NUMBER}]"

  if [ -z ${NEXT_PROJECT_VERSION} ] ; then
    NEXT_PROJECT_VERSION=`echo ${CLEANED} | sed -e "s/[0-9][0-9]*\([^0-9]*\)$/${NEXT_BUILD_NUMBER}/"`
  else
    echo "Version number was overridden on the command line. Using [${NEXT_PROJECT_VERSION}] to calculate next version."
    NEXT_PROJECT_VERSION="${NEXT_PROJECT_VERSION}"
  fi

  echo "Next project version: [${NEXT_PROJECT_VERSION}]"
}
#####################################################################################################################
function updateMvnProjectPomsToNextVersion() {
 cd $PROJECT_DIR
 echo "Updating project version to [${NEXT_PROJECT_VERSION}]..."
  $MVN ${MAVEN_VERSIONS_PLUGIN_SET_GOAL} -DnewVersion=${NEXT_PROJECT_VERSION}
  echo "Adding pom.xml to Git: "
  REPO=`echo $3 | awk -F/ '{print $1}'`
  echo "REPO: "$REPO
  BRANCH=`echo $3 | awk -F/ '{print $2}'`
  echo "BRANCH: "$BRANCH
  echo "ADDING NEW VERSION..."
  $FIND . -type f -name 'pom.xml' -exec $GIT add {} +
  echo "COMMIT NEW VERSION..."
  $FIND . -type f -name 'pom.xml' -exec $GIT  commit -m "Update version before deploy" {} +
  echo "PUSH NEW VERSION..."
  $GIT push $REPO $BRANCH
}
#####################################################################################################################
function cleanAndCheckout() {
	cd $1
	echo "REMOVE COPY!"
	rm -rf $1/*
	rm -rf $1/.git
	echo "CLONE COPY: "$2
	BRANCH=`echo $3 | awk -F/ '{print $2}'`
	echo "BRANCH NAME BEFORE CLONE: "$BRANCH
	git clone $2 $1 -b $BRANCH
}
#####################################################################################################################

#########MAIN#########
res=$(defineBuilder)

if [ $res == "mvn" ]; then
	cleanAndCheckout $1 $2 $3
	initMvnCurrentProjectVersion
	initMvnNextProjectVersion
	updateMvnProjectPomsToNextVersion $1 $2 $3
elif [ $res == "gradle" ]; then
	echo "GRADLE PROJECT"
	$GRADLE --version
	$GRADLE increment
	$GRADLE gitPublishPush
else
	exit 1
fi
